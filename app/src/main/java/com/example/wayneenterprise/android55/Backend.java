package com.example.wayneenterprise.android55;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 * @author Gabriel Gutierrez, Lyle Filonuk
 *
 * Stores all of the data for the application. Holds all the methods for creating new data, deleting new data,
 * serializing data, and deserializing data.
 */
public class Backend
{   
    //static final long serialVersionUID = 1L;

    private Context context;
    /**
     * list of users
     */
	public static User user;
    /**
     * directory of data file
     */
	private static final String dir = "data";
    /**
     * name of data file
     */
    private static final String filename = "data.dat";

    public Backend(Context c)
    {
        this.context = c;

        File f = new File(context.getFilesDir() + File.separator + filename);
        if(f.exists())
        {
            loadProject();
        }
        else
        {
            user = new User("username", "first last");
            save();
        }
    }

    /**
     * get directory of data file
     * @return directory name
     */
    public static String getDir()
    {
        return dir;
    }

    /**
     * get data file name
     * @return file name
     */
    public static String getFilename()
    {
        return filename;
    }

    /**
     * returns list of user
     * @return user list
     */
    public static User getUser()
    {
        return user;
    }

    /**
     * confirmation prompt
     * @param header, header for prompt
     * @param content, prompt message
     * @return user decision
     */
    public static boolean confirm(String header, String content)
    {
      return false;
    }

    /**
     * prompts user for input
     * @param title, prompt title
     * @param header, prompt header
     * @param content, prompt mesage
     * @return user's input
     */
    public static String inputPrompt(String title, String header, String content)
    {
        return null;
    }

    /**
     * prompts use of error
     * @param header, prompt header
     * @param content, prompt message
     */
    public static void errorPrompt(String header, String content) {
    }
    
    /**
     * Saves project
     *
     */
    public void save()
    {
        try
        {
            FileOutputStream fo = context.openFileOutput(filename, Context.MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fo);
            out.writeObject(user);
            out.close();
            fo.close();
            //System.out.println("User list saved.");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Loads Project
     */
    public void loadProject()
    {
        try
        {
            FileInputStream fis = context.openFileInput(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            user = (User) ois.readObject();
            if (user == null)
            {
                user = new User("name", "name");
            }
            ois.close();
            fis.close();
            //System.out.println("User list loaded");
        }
        catch(FileNotFoundException e)
        {
            user = new User("", "");
            //System.out.println("User list created");
        }
        catch(ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
