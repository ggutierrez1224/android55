package com.example.wayneenterprise.android55;
/**
 * Class to create photo objects
 * @author Gabriel Gutierrez and Lyle Filonuk
 */

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class Photo implements Serializable, Comparable<Photo>
{
    static final long serialVersionUID = 1L;

    /**
     * user given name for the photo
     */
    private String photoName;
    /**
     * path to photo
     */
    private String path;
    /**
     * caption of the photo
     */
    private String caption;
    /**
     * date to be used
     */
    private Calendar date;
    /**
     * list of albums photo is in
     */
    private ArrayList<Album> albumsIn;
    /**
     * list of tags photo has
     */
    private ArrayList<Tag> photoTags;

    File f;
    private long id;

    /**
     * constructor for photo class
     * @param photoName, name of the photo
     * @param path, file path to photo
     * @param caption, photo's caption
     */
    public Photo(String photoName, String path, long id ,String caption)
    {
        this.photoName = photoName;
        this.path = path;
        this.caption = caption;
        this.id = id;

        f = new File(path);
        this.date = Calendar.getInstance();
        this.date.setTimeInMillis(f.lastModified());
        this.date.set(Calendar.MILLISECOND, 0);

        albumsIn = new ArrayList<Album>();
        photoTags = new ArrayList<Tag>();
    }

    /**
     * gets the name of the photo
     * @return photo name
     */
    public String getPhotoName()
    {
        return  this.photoName;
    }
    /**
     * get the path to photo
     * @return, file path
     */
    public String getPath()
    {
        return this.path;
    }

    /**
     * get caption of photo
     * @return caption
     */
    public String getCaption()
    {
        return this.caption;
    }

    /**
     * get date of photo
     * @return date of phtot
     */
    public Calendar getDate()
    {
        return date;
    }

    /**
     * get list of albums photo is in
     * @return list of albums
     */
    public ArrayList<Album> getAlbumsIn()
    {
        return albumsIn;
    }

    /**
     * get list of tags of photo
     * @return tag list
     */
    public ArrayList<Tag> getPhotoTags()
    {
        return photoTags;
    }

    /**
     * sets name of the photo
     * @param photoName, name of the photo
     */
    public void setPhotoName(String photoName)
    {
        this.photoName = photoName;
    }
    /**
     * set caption of photo
     * @param caption, caption of photo
     */
    public void setCaption(String caption)
    {
        this.caption = caption;
    }

    /**
     * add a tag to photo
     * @param tag, tag to be added
     */
    public boolean addTag(Tag tag)
    {
        if(this.photoTags.isEmpty())
        {
            photoTags.add(tag);
            return true;
        }
        else
        {
            for (Tag t : photoTags)
            {
                if (t.equals(tag))
                {
                    return false;
                }
            }
            photoTags.add(tag);
        }
        Collections.sort(this.photoTags);
        return true;
    }

    /**
     * removes a tag from the photo
     * @param tag, tag to be removed
     */
    public boolean removeTag(Tag tag)
    {
        if(this.photoTags.isEmpty())
        {
            return true;
        }
        else
        {
            for (int i = 0; i < photoTags.size(); i++)
            {
                if (photoTags.get(i).equals(tag))
                {
                    photoTags.remove(i);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * checks if album as indicated tag
     * @param tag, tag to be checked
     * @return true if photo has tag, false otherwise
     */
    public boolean checkTag(Tag tag)
    {
        for(Tag t : photoTags)
        {
            if(t.equals(tag))
            {
                return true;
            }
        }
        return false;
    }

    public File getFile()
    {
        return this.f;
    }

    /**
     * overrids equals method to check photo equivalence by checking file path
     * @param o, photo object to be checked
     * @return true if equal, false otherwise
     */
    @Override
    public boolean equals(Object o)
    {
        //make sure it is photo instance
        if (o == null || !(o instanceof Photo))
        {
            return false;
        }
        //cast to photo
        Photo op = (Photo) o;

        //check paths
        return this.getPath().equals(op.getPath());
    }
    /**
     * overrides the toString method
     */
    @Override
    public String toString() {
    	return photoName;
    }
    /**
     * overrides the compareTo method
     */
    @Override
    public int compareTo(Photo p)
    {
        return this.date.compareTo(p.getDate());
    }
}
