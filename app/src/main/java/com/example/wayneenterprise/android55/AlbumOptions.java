package com.example.wayneenterprise.android55;

import android.Manifest;;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class AlbumOptions extends AppCompatActivity
{
    public static ListView list;
    public static int currPos;
    public long id;
    public static User user;
    public static Album album;
    public static ArrayList<Album> albs;
    public static Photo selectedPhoto;
    public static ArrayList<Photo> photoList;
    CustomBaseAdapter adapter;
    public int SELECT_IMAGE = 0;
    static final Integer READ_EXST = 0x4;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_options);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        user = PhotoAlbum.user;
        albs = new ArrayList<Album>(user.getAlbums());
        album = PhotoAlbum.selectedAlbum;

        TextView albIn = (TextView)findViewById(R.id.albIn);
        albIn.setText("Current Album: " + album.getAlbumName() + "\n");

        /* set up the list view to hold route names */
        list = (ListView)findViewById(R.id.photoList);

        // get the photos from the selected album
        photoList = new ArrayList<>(PhotoAlbum.selectedAlbum.getPhotoList());

        adapter = new CustomBaseAdapter(this, photoList);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> a, View v , int position, long id)
            {
                selectedPhoto = (Photo)photoList.get(position); // currently selected photo is set to variable
                currPos = position;
            }
        });
    }

  

    public void remove(View v)
    {
        //make sure a photo was selected
        if (selectedPhoto == null)
        {
            Toast.makeText(this, "No photo selected!", Toast.LENGTH_SHORT).show();

        }
        else
        {
            album.deletePhoto(selectedPhoto);
            photoList.remove(selectedPhoto);
            selectedPhoto = null;
            adapter.notifyDataSetChanged();
            PhotoAlbum.backend.save();
        }
    }

    public void display(View v)
    {
        if(selectedPhoto == null)
        {
            Toast.makeText(this, "No photo selected!", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Intent j = new Intent(AlbumOptions.this, PhotoDisplay.class);
            startActivity(j);
        }
    }

    public void add(View v)
    {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(
                    this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},READ_EXST);
        }


        Intent image=new Intent();
        image.setType("image/*");
        image.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(image,"select file"),SELECT_IMAGE);
    }

    public void move(View v) {

        if(selectedPhoto == null) {

            Toast.makeText(this, "No photo selected!", Toast.LENGTH_SHORT).show();

        }
        else
        {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(AlbumOptions.this);
                builderSingle.setTitle("Select One Name:-");

                final ArrayAdapter<Album> arrayAdapter = new ArrayAdapter<Album>(
                        AlbumOptions.this,
                        android.R.layout.select_dialog_singlechoice);
                arrayAdapter.addAll(user.getAlbums());

                builderSingle.setNegativeButton(
                        "cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                builderSingle.setAdapter(
                        arrayAdapter,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final Album destAlbum = arrayAdapter.getItem(which);
                                if(destAlbum.getAlbumName().equalsIgnoreCase(album.getAlbumName()))
                                {
                                    Toast.makeText(getApplicationContext(), "You can't move a photo to the same album!", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                AlertDialog.Builder builderInner = new AlertDialog.Builder(
                                        AlbumOptions.this);
                                builderInner.setMessage("Album selected: " + destAlbum.getAlbumName());
                                builderInner.setTitle("Move to");
                                builderInner.setPositiveButton(
                                        "Ok",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                // check if the photo already exists in the destination album or a photo with a duplicate name exists
                                                if(destAlbum.checkName(selectedPhoto.getPhotoName()) || !destAlbum.addPhoto(selectedPhoto))
                                                {
                                                    Toast.makeText(getApplicationContext(), "That photo already exists in the destination album or a photo has the same name", Toast.LENGTH_SHORT).show();
                                                    return;
                                                }
                                                // remove the photo from the this album
                                                album.deletePhoto(selectedPhoto);
                                                photoList.remove(selectedPhoto);
                                                selectedPhoto = null;
                                                adapter.notifyDataSetChanged();
                                                PhotoAlbum.backend.save();
                                            }
                                        }).setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                    }});
                                builderInner.show();
                            }
                        });
                builderSingle.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_IMAGE)
        {
            if( resultCode == RESULT_OK && data != null)
            {
                Uri selectedImage = data.getData();
                final String picturePath = getImagePath(selectedImage);

                //get a name for the photo
                final EditText pName = new EditText(this);
                new AlertDialog.Builder(this)
                        .setTitle("New Photo")
                        .setMessage("Enter photo name:")
                        .setView(pName)
                        .setPositiveButton("Add Photo", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String name = pName.getText().toString().trim();

                                //check if photo name already exists, if not create it
                                if(!album.checkName(name) && !name.isEmpty())
                                {
                                    //final String ph = picturePath;
                                    Photo p = new Photo(name, picturePath, id,"");
                                    if(!album.addPhoto(p))
                                    {
                                        Toast.makeText(getApplicationContext(), "Photo already exists!", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    photoList.add(p);
                                    adapter.notifyDataSetChanged();
                                    PhotoAlbum.backend.save();
                                }
                                else if(name.isEmpty())
                                {
                                    Toast.makeText(getApplicationContext(), "No name entered!\nUnable to add photo!", Toast.LENGTH_SHORT).show();
                                }
                                //prompt error that photo name is taken
                                else
                                {
                                    Toast.makeText(getApplicationContext(), "Photo name is taken!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }}).show();
            }
        }
    }

    public String getImagePath(Uri uri)
    {
        //String[] columns = {MediaStore.MediaColumns._ID};
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
        id = cursor.getLong(0);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED)
        {
            //Read External Storage
            Intent imageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            //startActivityForResult(imageIntent, 11);
            Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }
}
