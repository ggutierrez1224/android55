package com.example.wayneenterprise.android55;

import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PhotoDisplay extends AppCompatActivity
{
    public int currPos;
    public int size;
    public Album selectedAlbum;
    public Photo selectedPhoto;
    public Photo current;
    public ArrayList<Tag> tagList;
    public Tag selectedTag;
    ArrayAdapter<Tag> adapter;

    public Spinner tagType;
    public EditText tagValue;
    public ImageView imgDisplay;
    public ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_display);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if(PhotoAlbum.results != null)
        {
            this.selectedPhoto = SearchResults.selectedPhoto;
            size = SearchResults.photoList.size();
        }
        else
        {
            this.selectedPhoto = AlbumOptions.selectedPhoto;
            size = AlbumOptions.photoList.size();
        }
        current = selectedPhoto;
        this.currPos = AlbumOptions.currPos;

        TextView name = (TextView) findViewById(R.id.photoName);
        name.setText("Photo name: " + selectedPhoto.getPhotoName());


        tagType = (Spinner)findViewById(R.id.tagType);
        tagValue = (EditText)findViewById(R.id.tagValue);

        // Display the selectedPhoto
        ImageView ImgView = (ImageView)findViewById(R.id.imgDisplay);
        ImgView.setImageBitmap(BitmapFactory.decodeFile(selectedPhoto.getPath()));


        listView = (ListView)findViewById(R.id.tagList);

        // get the photos from the selected album
        tagList = new ArrayList<>(selectedPhoto.getPhotoTags());

        //display it onto the app
        adapter = new ArrayAdapter<Tag>(this, android.R.layout.simple_list_item_1, tagList);
        listView.setAdapter(adapter);

        adapter.notifyDataSetChanged();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> a, View v , int position, long id)
            {
                selectedTag = (Tag) listView.getItemAtPosition(position);
            }
        });
    }

    public void addTag(View v)
    {
        //get input
        String type = String.valueOf(tagType.getSelectedItem()).trim();
        String value = tagValue.getText().toString().trim();

        //make sure there is no empty input
        if(type.isEmpty() || value.isEmpty())
        {
            Toast.makeText(this, "Input missing!", Toast.LENGTH_SHORT).show();
            return;
        }

        //create tag
        Tag tag = new Tag(type, value);
        tagType.setSelection(0);
        tagValue.setText("");

        //check if it exists in photo
        if(!current.addTag(tag))
        {
            Toast.makeText(this, "Photo already has this tag!", Toast.LENGTH_SHORT).show();
        }
        else
        {
            tagList.clear();
            tagList.addAll(current.getPhotoTags());
            adapter.notifyDataSetChanged();
            PhotoAlbum.backend.save();
        }
    }

    public void deleteTag(View v)
    {
        if(selectedTag == null)
        {
            Toast.makeText(this, "No tag selected!", Toast.LENGTH_SHORT).show();
            return;
        }

        if(current.removeTag(selectedTag))
        {
            tagList.clear();
            tagList.addAll(current.getPhotoTags());
            adapter.notifyDataSetChanged();
            PhotoAlbum.backend.save();
            selectedTag = null;
        }
    }

    public void next(View v)
    {
        if (currPos + 1 < size)
        {
            if(PhotoAlbum.results != null)
            {
                current = (Photo) SearchResults.list.getItemAtPosition(currPos + 1);
            }
            else
            {
                current = (Photo) AlbumOptions.list.getItemAtPosition(currPos + 1);
            }

            //display next image to image view
            imgDisplay = (ImageView)findViewById(R.id.imgDisplay);
            imgDisplay.setImageBitmap(BitmapFactory.decodeFile(current.getPath()));

            //set text view to name of next image
            TextView myTextView = (TextView) findViewById(R.id.photoName);
            myTextView.setText("Photo name: " + current.getPhotoName());

            //update tag list
            adapter.clear();
            adapter.addAll(current.getPhotoTags());
            adapter.notifyDataSetChanged();

            //change current position
            currPos = currPos + 1;
        }
        //reached end of photo list, wrap back around to the beginning
        else
        {
            currPos = -1;
            next(v);
        }
    }

    public void previous(View v)
    {
        if(currPos - 1 >= 0)
        {
            if(PhotoAlbum.results != null)
            {
                current = (Photo) SearchResults.list.getItemAtPosition(currPos - 1);
            }
            else
            {
                current = (Photo) AlbumOptions.list.getItemAtPosition(currPos - 1);
            }

            //display previous image to image view
            imgDisplay = (ImageView)findViewById(R.id.imgDisplay);
            imgDisplay.setImageBitmap(BitmapFactory.decodeFile(current.getPath()));

            //set text view to name of previous image
            TextView myTextView = (TextView) findViewById(R.id.photoName);
            myTextView.setText("Photo name: " + current);

            //update tag list
            adapter.clear();
            adapter.addAll(current.getPhotoTags());
            adapter.notifyDataSetChanged();

            //update position
            currPos = currPos - 1;
        }
        //reached beginning of photo list, wrap back around to the end
        else
        {
            currPos = size;
            previous(v);
        }
    }
}
