package com.example.wayneenterprise.android55;

import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomBaseAdapter extends BaseAdapter
{
    Context context;
    List<Photo> rowItems;

    public CustomBaseAdapter(Context context, List<Photo> items)
    {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
    private class ViewHolder
    {
        ImageView img;
        TextView name;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = mInflater.inflate(R.layout.img_txt_list, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.img = (ImageView) convertView.findViewById(R.id.thumb);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        Photo photo = (Photo)getItem(position);

        holder.name.setText(photo.getPhotoName());
        holder.img.setImageBitmap(BitmapFactory.decodeFile(photo.getPath()));

        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position)
    {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return rowItems.indexOf(getItem(position));
    }
}
