package com.example.wayneenterprise.android55;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class SearchResults extends AppCompatActivity
{
    public static ListView list;
    public static int currPos;
    public long id;
    public static User user;
    public static Album album;
    public static Photo selectedPhoto;
    public static ArrayList<Photo> photoList;
    CustomBaseAdapter adapter;
    public int SELECT_IMAGE = 0;
    static final Integer READ_EXST = 0x4;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        /* set up the list view to hold route names */
        list = (ListView)findViewById(R.id.photoList);

        // get the photos from the selected album
        photoList = new ArrayList<>(PhotoAlbum.results);

        adapter = new CustomBaseAdapter(this, photoList);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> a, View v , int position, long id)
            {
                selectedPhoto = (Photo)photoList.get(position);
                currPos = position;
            }
        });
    }

    public void display(View v)
    {
        if(selectedPhoto == null)
        {
            Toast.makeText(this, "No photo selected!", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Intent j = new Intent(SearchResults.this, PhotoDisplay.class);
            startActivity(j);
        }
    }
}
