package com.example.wayneenterprise.android55;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class PhotoAlbum extends AppCompatActivity
{
    public EditText albSearch;
    public ListView albs;

    public static User user;
    public static Backend backend;
    public static ArrayList<Album> albList;
    public static ArrayList<Photo> photoList;
    public static ArrayList<Photo> results;
    public static Album selectedAlbum;
    ArrayAdapter<Album> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_album);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Context ctx;
        ctx = this;
        backend = new Backend(ctx);

        //load project
        backend.loadProject();
        user = backend.getUser();
        albList = new ArrayList<>(user.getAlbums());
        results = null;

        //set up list view
        albs = (ListView)findViewById(R.id.albList);

        //print albums to list view
        adapter = new ArrayAdapter<Album>(this, android.R.layout.simple_list_item_activated_1, albList);
        albs.setAdapter(adapter);

        adapter.notifyDataSetChanged();

        //make listener to get selected album
        albs.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> a, View v , int position, long id)
            {
                selectedAlbum = (Album) albs.getItemAtPosition(position);
            }
        });
    }

    public void rename(View v)
    {
        final EditText albName = new EditText(this);

        albName.setHint("Album Name");

        new AlertDialog.Builder(this)
                .setTitle("Rename Album")
                .setMessage("Enter new album name:")
                .setView(albName)
                .setPositiveButton("Rename", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String name = albName.getText().toString();

                        //check if album name already exists
                        if(!name.isEmpty() &&!user.checkName(name) && !selectedAlbum.getAlbumName().equals(name))
                        {
                            selectedAlbum.setAlbumName(name);
                            adapter.notifyDataSetChanged();
                            backend.save();
                        }
                        //prompt error
                        else if(name.isEmpty())
                        {
                            Toast.makeText(getApplicationContext(), "Empty input!", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Album name is taken!", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .show();
    }

    public void create(View v)
    {
        final EditText albName = new EditText(this);

        albName.setHint("Album Name");

        new AlertDialog.Builder(this)
                .setTitle("Create Album")
                .setMessage("Enter album name:")
                .setView(albName)
                .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String name = albName.getText().toString();

                        //check if album name already exists, if not create it
                        if(!name.isEmpty() &&!user.checkName(name))
                        {
                            //create album
                            user.addAlbum(name);
                            albList.add(user.getAlbum(name));
                            adapter.notifyDataSetChanged();
                            backend.save();
                        }
                        //prompt error that album name is taken
                        else if(name.isEmpty())
                        {
                            Toast.makeText(getApplicationContext(), "Empty input!", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Album name is taken!", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .show();
    }

    public void open(View v)
    {
        if (selectedAlbum != null)
        {
            backend.save();
            results = null;
            Intent i = new Intent(PhotoAlbum.this, AlbumOptions.class);
            startActivity(i);
        }
        else
        {
            Toast.makeText(getApplicationContext(), "No album selected!", Toast.LENGTH_SHORT).show();
        }
    }

    public void delete(View v)
    {
        if(selectedAlbum != null && user.checkName(selectedAlbum.getAlbumName()))
        {
            user.removeAlbum(selectedAlbum);
            albList.remove(selectedAlbum);
            adapter.notifyDataSetChanged();
            backend.save();
        }
    }

    public void search(View v)
    {
        EditText et = (EditText)findViewById(R.id.tagSearch);
        Spinner s = (Spinner)findViewById(R.id.tagType);

        String type = String.valueOf(s.getSelectedItem()).trim();
        String value = et.getText().toString().trim();
        et.setText("");

        if(type.isEmpty() || value.isEmpty())
        {
            Toast.makeText(this, "Input missing!", Toast.LENGTH_SHORT).show();
            return;
        }

        results = new ArrayList<Photo>();

        ArrayList<Tag> tags;

        for(Album a : albList)
        {
            photoList = a.getPhotoList();
            for(Photo p : photoList)
            {
                tags = p.getPhotoTags();
                for(Tag t : tags)
                {
                    if(t.getType().equalsIgnoreCase(type) && t.getValue().contains(value))
                    {
                        results.add(p);
                    }
                }
            }
        }


        if(results.isEmpty())
        {
            Toast.makeText(this, "No photos found!", Toast.LENGTH_SHORT).show();
            results = null;
        }
        else
        {
            PhotoAlbum.backend.save();
            Intent i = new Intent(PhotoAlbum.this, SearchResults.class);
            startActivity(i);
        }
    }
}
