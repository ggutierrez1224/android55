package com.example.wayneenterprise.android55;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;

public class Album implements Serializable, Comparator<Album>, Comparable<Album>
{
    static final long serialVersionUID = -5496597739575537038L;

    /**
     * list of photos in album
     */
    private ArrayList<Photo> photoList;
    /**
     * name of album
     */
    private String albumName;
    /**
     * user that owns this album
     */
    private User user;
    /**
     * number of photos in album
     */
    private int numPhotos;
    /**
     * most recent photo date in the album
     */
    private Calendar newestDate;
    /**
     * oldest photo date in the album
     */
    private Calendar oldestDate;

    /**
     * constructor for Album
     * @param name, name of album
     * @param user, user that owns the album
     */
    public Album(String name, User user)
    {
        this.albumName = name;
        this.user = user;
        photoList = new ArrayList<Photo>();
        numPhotos = 0;
        this.oldestDate = null;
        this.newestDate = null;
    }

    /**
     * add a photo to the album
     * @param photo, photo to be added
     */
    public boolean addPhoto (Photo photo)
    {
        //check for null list
        if (photoList == null)
        {
            //initialize
            photoList = new ArrayList<Photo>();
            //add photo to list
            photoList.add(photo);
        }
        //check list if photo already exists
        for (Photo p : photoList)
        {
            if (p.equals(photo))
            {
                //prompt error to user
                return false;
            }
        }
        //photo does not exist, add it
        this.photoList.add(photo);
        numPhotos++;

        // adjust newest and oldest photo dates
        if (oldestDate == null || photo.getDate().compareTo(oldestDate) < 0)
        {
            setOldestDate(photo.getDate());
        }

        if (newestDate == null || photo.getDate().compareTo(newestDate) > 0)
        {
            setNewestDate(photo.getDate());
        }
        return true;
    }

    /**
     * delete a photo from the album
     * @param photo, photo to be deleted
     */
    public boolean deletePhoto(Photo photo)
    {
        //search list for photo
        for(Photo p : this.getPhotoList())
        {
            photoList.remove(photo);
            numPhotos--;
            return true;
        }
        //does not exist in list, prompt user
        return false;
    }

    /**
     * searches for a photo
     * @param photoName, name of photo being searched
     * @return photo if found
     */
    public Photo getPhoto(String photoName)
    {
        //check for null list
        if(photoList == null)
        {
            return null;
        }
        //search list by photo name
        for(Photo p : photoList)
        {
            if(p.getPhotoName().equals(photoName))
            {
                return p;
            }
        }
        return null;
    }

    /**
     * set the name of the album
     * @param albumName, name of the album
     */
    public void setAlbumName(String albumName)
    {
        this.albumName = albumName;
    }

    /**
     * set a user to the album
     * @param user, user to be set
     */
    public void setUser(User user)
    {
        this.user = user;
    }

    /**
     * set oldest photo date in album
     * @param oldestDate, date to be set
     */
    public void setOldestDate(Calendar oldestDate)
    {
        this.oldestDate = oldestDate;
    }

    /**
     * set newest photo date in album
     * @param newestDate, date to be set
     */
    public void setNewestDate(Calendar newestDate)
    {
        this.newestDate = newestDate;
    }

    /**
     * get name of album
     * @return the album name
     */
    public String getAlbumName()
    {
        return this.albumName;
    }

    public boolean checkName(String pName)
    {
        for(Photo p : photoList)
        {
            //if name matches, return it
            if(p.getPhotoName().equalsIgnoreCase(pName))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * returns earliest photo date
     * @return  earliest photo date
     */
    public Calendar getNewestDate()
    {
        return this.newestDate;
    }

    /**
     * get oldest photo date
     * @return oldest photo date
     */
    public Calendar getOldestDate()
    {
        return this.oldestDate;
    }
    /**
     * get number of photos in album
     * @return number of photos
     */
    public int getNumPhotos()
    {
        return numPhotos;
    }
    /**
     * set the num of photos for the album
     * @param num
     */
    public void setNumPhotos(int num) {
    	numPhotos = num;
    }
    /**
     * get the user that owns the album
     * @return the user
     */
    public User getUser()
    {
        return this.user;
    }
    
    /**
     * @return the ArrayList of Photos
     */
    public ArrayList<Photo> getPhotoList() {
    	return photoList;
    }
    /**
     * compares album name
     * @param a, other album
     * @return return value of string's compareTo
     */
    public int compareTo(Album a)
    {
        return this.albumName.compareTo(a.getAlbumName());
    }

    /**
     * compares two albums by name
     * @param a, album
     * @param b, album
     * @return return from compareTo
     */
    public int compare(Album a, Album b)
    {
        return a.getAlbumName().compareTo(b.getAlbumName());
    }

    @Override
    public String toString()
    {
        return getAlbumName();
    }
}